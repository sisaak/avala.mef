﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avala.Mef.Csi.Services.Core
{
    public interface IRegistrationService
    {
        string GetName();
    }
}
