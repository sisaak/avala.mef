﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Avala.Mef.Csi.Services;
using Avala.Mef.Csi.Services.Core;
using Microsoft.Practices.Unity;

namespace Avala.Mef.Csi.CrossCutting
{
    public class Container
    {

        public void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<IRegistrationService, RegistrationService>();
        }
    }
}
