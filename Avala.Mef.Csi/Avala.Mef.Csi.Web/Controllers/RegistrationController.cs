﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Avala.Mef.Csi.Services.Core;
using Avala.Mef.Framework.Core.Web;

namespace Avala.Mef.Csi.Web.Controllers
{
    [ExportController("Registration", "CSI")]
    public class RegistrationController : Controller
    {
        private IRegistrationService _registrationService;

        [ImportingConstructor]
        public RegistrationController(IRegistrationService registrationService)
        {
            if (registrationService == null)
                throw new ArgumentNullException("registrationService");

            _registrationService = registrationService;
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Registrations";
            ViewBag.Message = "Registration Index";
            return View();
        }

        public ActionResult Home()
        {
            ViewBag.Title = "CSI";
            ViewBag.Message = _registrationService.GetName();
            return View();
        }
    }
}
