﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Avala.Mef.Csi.Services.Core;
using Avala.Mef.Framework.Core.Web;

namespace Avala.Mef.Csi.Web.Controllers
{
    [ExportController("Survey", "CSI")]
    public class SurveyController : Controller
    {
        public SurveyController()
        {
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Surveys";
            ViewBag.Message = "Survey Index";
            return View();
        }
    }
}
