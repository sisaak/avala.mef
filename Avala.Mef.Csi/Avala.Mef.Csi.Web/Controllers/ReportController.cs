﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Avala.Mef.Csi.Services.Core;
using Avala.Mef.Framework.Core.Web;

namespace Avala.Mef.Csi.Web.Controllers
{
    [ExportController("Report", "CSI")]
    public class ReportController : Controller
    {
        public ReportController()
        {
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Reports";
            ViewBag.Message = "Report Index";
            return View();
        }

        public ActionResult Report1()
        {
            ViewBag.Title = "Report 1";
            ViewBag.Message = "Report 1";
            return View();
        }
    }
}
