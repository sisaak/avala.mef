﻿using System.ComponentModel.Composition;
using System.Web.Mvc;

namespace Avala.Mef.Csi.Web
{
    [Export(typeof(AreaRegistration))]
    public class CSIAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "CSI";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "CSI_default",
                "CSI/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
