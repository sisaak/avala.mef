﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Avala.Mef.Csi.CrossCutting;
using Avala.Mef.Framework.Core.Injection;
using Microsoft.Practices.Unity;

namespace Avala.Mef.Csi.Web
{
    [Export(typeof(IModuleContainer)), ExportMetadata("Name", "CSI")]
    public class ModuleContainer : IModuleContainer
    {
        private Container _container = new Container(); 

        public void RegisterAllTypes(IUnityContainer container)
        {
            _container.RegisterTypes(container);
        }
    }
}
