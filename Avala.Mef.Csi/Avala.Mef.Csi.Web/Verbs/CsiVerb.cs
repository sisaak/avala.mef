﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Avala.Mef.Framework.Core.Web;

namespace Avala.Mef.Csi.Web.Verbs
{
    [Export(typeof(IActionVerb)), ExportMetadata("Category", "Navigation"), ExportMetadata("Order", 2)]
    public class CsiVerb : ActionVerb
    {
        public CsiVerb()
        {
            Name = "CSI";
            Action = "Home";
            Controller = "Registration";
            Area = "CSI";
            Children = new List<IActionVerb>
            {
                new ActionVerb { Name = "Registrations", Action = "Index", Controller = "Registration", Area = "CSI" },
                new ActionVerb { Name = "Surveys", Action = "Index", Controller = "Survey", Area = "CSI" },
                new ActionVerb { Name = "Reports", Action = "Index", Controller = "Report", Area = "CSI", Children = new List<IActionVerb> {
                    new ActionVerb { Name = "Report 1", Action = "Report1", Controller = "Report", Area = "CSI" }
                }}
            };
        }
    }
}
