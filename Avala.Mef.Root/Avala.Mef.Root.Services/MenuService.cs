﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Avala.Mef.Root.Services.Core;

namespace Avala.Mef.Root.Services
{
    public class MenuService : IMenuService
    {
        public IEnumerable<MenuDto> GetMenu()
        {
            return new List<MenuDto>
            {
                new MenuDto { Text = "Home", Controller = "Home", Action = "Index", Children = new List<MenuDto> () },
                new MenuDto { Text = "Sales", Area = "Lead", Controller = "Home", Action = "Index", Children = new List<MenuDto> {
                        new MenuDto { Text = "Leads", Area = "Lead", Controller = "Lead", Action = "Index", Children = new List<MenuDto>() },
                        new MenuDto { Text = "Prospects", Area = "Lead", Controller = "Prospect", Action = "Index", Children = new List<MenuDto>() },
                        new MenuDto { Text = "Products", Area = "Lead", Controller = "Product", Action = "Index", Children = new List<MenuDto>() },
                        new MenuDto { Text = "Reports", Area = "Lead", Controller = "Report", Action = "Index", Children = new List<MenuDto> {
                            new MenuDto { Text = "Lead Reports by Source", Area =  "Lead", Controller = "Report", Action = "BySource", Children = new List<MenuDto>() }
                        } }
                    }
                },
                new MenuDto { Text = "CSI", Area = "Owner", Controller = "Home", Action = "Index", Children = new List<MenuDto> { 
                        new MenuDto { Text = "Registrations", Area = "Owner", Controller = "Registration", Action = "Index", Children = new List<MenuDto>() },
                        new MenuDto { Text = "Owners", Area = "Owner", Controller = "Owner", Action = "Index", Children = new List<MenuDto>() },
                        new MenuDto { Text = "Surveys", Area = "Owner", Controller = "Survey", Action = "Index", Children = new List<MenuDto>() }
                    }
                }
            };
        }
    }
}
