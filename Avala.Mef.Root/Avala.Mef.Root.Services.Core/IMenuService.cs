﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avala.Mef.Root.Services.Core
{
    public interface IMenuService
    {
        IEnumerable<MenuDto> GetMenu();
    }
}
