﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Avala.Mef.Root.Services;
using Avala.Mef.Root.Services.Core;

namespace Avala.Mef.Root.Web.Controllers
{
    public class HomeController : Controller
    {
        private IMenuService _menuService;

        public HomeController()
        {
            _menuService = new MenuService();
        }

        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";
            ViewBag.Menu = _menuService.GetMenu();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";
            ViewBag.Menu = _menuService.GetMenu();
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            ViewBag.Menu = _menuService.GetMenu();
            return View();
        }
    }
}
