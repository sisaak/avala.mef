﻿using System.Web;
using System.Web.Mvc;

namespace Avala.Mef.Root.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}