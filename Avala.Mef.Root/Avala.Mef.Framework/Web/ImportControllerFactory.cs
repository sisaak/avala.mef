﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Avala.Mef.Framework.Extensions;
using Avala.Mef.Framework.Composition;
using Avala.Mef.Framework.Core.Web;

namespace Avala.Mef.Framework.Web
{
    /// <summary>
    /// Provides creation of controllers using imports.
    /// </summary>
    [Export]
    public class ImportControllerFactory : DefaultControllerFactory
    {
        #region Fields
        [ImportMany] private IEnumerable<PartFactory<IController, IControllerMetadata>> ControllerFactories;
        #endregion

        #region Methods
        /// <summary>
        /// Creates an instance of a controller for the specified name.
        /// </summary>
        /// <param name="requestContext">The current request context.</param>
        /// <param name="controllerName">The name of the controller.</param>
        /// <returns>An instance of a controller for the specified name.</returns>
        public override IController CreateController(System.Web.Routing.RequestContext requestContext, string controllerName)
        {
            //get factories with matching name
            var factories = ControllerFactories.Where(f => f.Metadata.Name.Equals(controllerName, StringComparison.InvariantCultureIgnoreCase)).ToList();

            //get factory with matching area
            var factory = factories.Where(f => (f.Metadata.Area == null && !requestContext.RouteData.DataTokens.ContainsKey("area")) || 
                (f.Metadata.Area.Equals((string)requestContext.RouteData.DataTokens["area"], StringComparison.InvariantCultureIgnoreCase)))
                .FirstOrDefault();

            if (factory != null)
                return factory.CreatePart();

            return base.CreateController(requestContext, controllerName);
        }
        #endregion
    }
}
