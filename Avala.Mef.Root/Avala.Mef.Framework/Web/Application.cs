﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Avala.Mef.Framework.Composition;
using Avala.Mef.Framework.Configuration;
using Avala.Mef.Framework.Core.Web;
using Avala.Mef.Framework.Extensions;
using System.Web.Http;
using Microsoft.Practices.Unity;
using Avala.Mef.Framework.Core.Injection;
using Avala.Mef.Framework.Core.Composition;

namespace Avala.Mef.Framework.Web
{
    public class Application : HttpApplication
    {
        #region Fields

        [ImportMany]private IEnumerable<Lazy<IRouteRegistrar, IRouteRegistrarMetadata>> RouteRegistrars;
        [ImportMany]private IEnumerable<Lazy<AreaRegistration>> AreaRegistrations;
        [ImportMany]private IEnumerable<Lazy<IModuleContainer>> ModuleContainers;
        private static IEnumerable<Lazy<IActionVerb, IActionVerbMetadata>> ActionVerbs;

        [Import] private ImportControllerFactory ControllerFactory;

        public static Composer Composer { get; private set; }
        protected IUnityContainer unityContainer;
        protected CSLExportProvider exportProvider;

        #endregion Fields

        #region Methods
        /// <summary>
        /// The start method of the application.
        /// </summary>
        protected void Application_Start()
        {
            // Perform any tasks required before composition.
            PreCompose();

            //create the composer
            Composer = CreateComposer();

            // Compose the application.
            Compose();

            // Register all types
            RegisterTypes();

            // Re-Compose the application now that all of the submodule's types have been registered
            Composer = CreateComposer();
            Compose();

            // Set the controller factory.
            ControllerBuilder.Current.SetControllerFactory(ControllerFactory);

            // Set the view engine that supports imported areas.
            ViewEngines.Engines.Add(new AreaViewEngine());

            // Initialises the application.
            Initialise();

            // Register all areas
            RegisterAreas();

            // Register MVC routes.
            RegisterRoutes();
        }

        /// <summary>
        /// Initialises the application.
        /// </summary>
        protected virtual void Initialise() { }

        /// <summary>
        /// Creates the composition container.
        /// </summary>
        /// <returns></returns>
        protected virtual CompositionContainer CreateCompositionContainer()
        {
            var catalog = new AggregateCatalog();
            catalog.Catalogs.Add(new DirectoryCatalog(MapPath("~/bin")));

            var config = CompositionConfigurationSection.GetInstance();
            if (config != null && config.Catalogs != null) {
                config.Catalogs
                    .Cast<CatalogConfigurationElement>()
                    .ForEach(c =>
                    {
                        if (!string.IsNullOrEmpty(c.Path)) {
                            string path = c.Path;
                            if (path.StartsWith("~"))
                                path = MapPath(path);

                            foreach (var directoryCatalog in GetDirectoryCatalogs(path))
                                catalog.Catalogs.Add(directoryCatalog);
                        }
                    });
            }

            var provider = new DynamicInstantiationExportProvider();
            var container = new CompositionContainer(catalog, provider);
            provider.SourceProvider = container;

            return container;
        }

        /// <summary>
        /// Fired before the application is composed.
        /// </summary>
        protected virtual void PreCompose()
        {
            unityContainer = new UnityContainer();

            var locator = new UnityServiceLocator(unityContainer);
            exportProvider = new CSLExportProvider(locator);

            unityContainer.AddExtension(new CSLExportProviderExtension(exportProvider));
        }

        protected virtual Composer CreateComposer()
        {
            var composer = new Composer();
            GetDirectoryCatalogs().ForEach(catalog => composer.AddCatalog(catalog));

            composer.AddExportProvider(new DynamicInstantiationExportProvider(), (provider, container) => ((DynamicInstantiationExportProvider)provider).SourceProvider = container);
            composer.AddExportProvider(exportProvider);

            return composer;
        }

        /// <summary>
        /// Composes the application.
        /// </summary>
        protected virtual void Compose()
        {
            if (Composer == null)
                return;

            Composer.Compose(this);

            ActionVerbs = Composer.ResolveAll<IActionVerb, IActionVerbMetadata>();
        }

        protected virtual void RegisterTypes()
        {
            if (ModuleContainers == null || !ModuleContainers.Any())
                return;

            ModuleContainers.ForEach(m => m.Value.RegisterAllTypes(unityContainer));
        }

        /// <summary>
        /// Registers any routes required by the application.
        /// </summary>
        protected virtual void RegisterRoutes()
        {
            if (RouteRegistrars == null || !RouteRegistrars.Any())
                return;

            var routes = RouteTable.Routes;

            var registrars = RouteRegistrars
                .OrderBy(lazy => lazy.Metadata.Order)
                .Select(lazy => lazy.Value);

            registrars.ForEach(r => r.RegisterIgnoreRoutes(routes));
            registrars.ForEach(r => r.RegisterRoutes(routes));
        }


        protected virtual void RegisterAreas()
        {
            if (AreaRegistrations == null || !AreaRegistrations.Any())
                return;

            AreaRegistrations.ForEach(areaRegistration =>
                {
                    var context = new AreaRegistrationContext(areaRegistration.Value.AreaName, RouteTable.Routes);
                    areaRegistration.Value.RegisterArea(context);
                });
        }

        /// <summary>
        /// Maps the specified virtual path.
        /// </summary>
        /// <param name="virtualPath">The virtual path to map.</param>
        /// <returns>The specified virtual path as a mapped path.</returns>
        protected string MapPath(string virtualPath)
        {
            if(string.IsNullOrEmpty(virtualPath))
                throw new ArgumentNullException("virtualPath");

            return HttpContext.Current.Server.MapPath(virtualPath);
        }

        /// <summary>
        /// Registers the specified path for probing.
        /// </summary>
        /// <param name="path">The probable path.</param>
        protected void RegisterPath(string path)
        {
#pragma warning disable 612,618
            AppDomain.CurrentDomain.AppendPrivatePath(path);
#pragma warning restore 612,618
        }

        /// <summary>
        /// Gets a set of <see cref="DirectoryCatalog" /> for the specified path and it's immediate child directories.
        /// </summary>
        /// <param name="path">The starting path.</param>
        /// <returns>An <see cref="IEnumerable{DirectoryCatalog}" /> of directory catalogs.</returns>
        protected IEnumerable<DirectoryCatalog> GetDirectoryCatalogs(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                throw new ArgumentNullException("path");

            List<DirectoryCatalog> list = new List<DirectoryCatalog>();
            list.Add(new DirectoryCatalog(path));

            list.AddRange(
                Directory.GetDirectories(path).Select(directory => new DirectoryCatalog(directory)));

            return list;
        }

        /// <summary>
        /// Gets an list of <see cref="DirectoryCatalog" />s of configured directories.
        /// </summary>
        /// <returns>An lsit of <see cref="DirectoryCatalog" />s of configured directories.</returns>
        protected IEnumerable<DirectoryCatalog> GetDirectoryCatalogs()
        {
            var list = new List<DirectoryCatalog>();
            string applicationPath = MapPath("~/");

            GetDirectoryCatalogs(MapPath("~/bin")).ForEach(catalog => list.Add(catalog));

            var config = CompositionConfigurationSection.GetInstance();
            if (config != null && config.Catalogs != null)
            {
                config.Catalogs
                    .Cast<CatalogConfigurationElement>()
                    .ForEach(c =>
                    {
                        if (!string.IsNullOrEmpty(c.Path))
                        {
                            string path = c.Path;
                            if (path.StartsWith("~") || path.StartsWith("/"))
                                path = MapPath(path);

                            GetDirectoryCatalogs(path)
                                .ForEach(catalog =>
                                {
                                    list.Add(catalog);
                                    RegisterPath(catalog.FullPath);
                                });
                        }
                    });
            }

            return list;
        }

        /// <summary>
        /// Gets the available verbs for the given category.
        /// </summary>
        /// <param name="category">The category of verbs to get.</param>
        /// <returns>An enumerable of verbs.</returns>
        public static IEnumerable<IActionVerb> GetVerbsForCategory(string category)
        {
            if(string.IsNullOrEmpty(category))
                throw new ArgumentNullException("category");

            return ActionVerbs
                .Where(l => l.Metadata.Category.Equals(category, StringComparison.InvariantCultureIgnoreCase))
                .OrderBy(l => l.Metadata.Order)
                .Select(l => l.Value);
        }
        #endregion
    }
}
