﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Avala.Mef.Framework.Web
{
    /// <summary>
    /// Provides view engine support for imported areas.
    /// </summary>
    public class AreaViewEngine : RazorViewEngine
    {
        #region Constructor
        /// <summary>
        /// Initialises a new instance of <see cref="AreaViewEngine" />.
        /// </summary>
        public AreaViewEngine()
        {
            MasterLocationFormats = new[]
                                    {
                                        "~/Views/{1}/{0}.cshtml",
                                        "~/Views/Shared/{0}.cshtml"
                                    };

            ViewLocationFormats = new[]
                                    {
                                        "~/Views/{1}/{0}.cshtml",
                                        "~/Views/Shared/{0}.cshtml"
                                    };
            AreaMasterLocationFormats = new[]
                                    {
                                        "~/Views/Shared/{0}.cshtml",
                                    };

            AreaViewLocationFormats = new[]
                                    {
                                        "~/Areas/{2}/Views/Shared/{0}.cshtml",
                                        "~/Areas/{2}/Views/{1}/{0}.cshtml"
                                    };

            AreaPartialViewLocationFormats = new[]
                                    {
                                        "~/Views/{1}/{0}.cshtml",
                                        "~/Views/Shared/{0}.cshtml"
                                    };
        }
        #endregion
    }
}
