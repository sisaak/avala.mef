﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Avala.Mef.Framework.Core.Composition;
using Microsoft.Practices.Unity;

namespace Avala.Mef.Framework.Composition
{
    public class CSLExportProviderExtension : UnityContainerExtension
    {
        private CSLExportProvider exportProvider;

        public CSLExportProviderExtension(CSLExportProvider exportProvider)
        {
            this.exportProvider = exportProvider;
        }

        protected override void Initialize()
        {
            this.Context.Registering += new EventHandler<RegisterEventArgs>(Context_Registering);
            this.Context.RegisteringInstance += new EventHandler<RegisterInstanceEventArgs>(Context_RegisteringInstance);
        }

        void Context_RegisteringInstance(object sender, RegisterInstanceEventArgs e)
        {
            exportProvider.RegisterType(e.RegisteredType);
        }


        void Context_Registering(object sender, RegisterEventArgs e)
        {
            exportProvider.RegisterType(e.TypeFrom);
        }
    }
}
