﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avala.Mef.Framework.Composition
{
    public class PartFactory<TPart>
    {
        #region Fields
        private readonly Func<PartLifetimeContext<TPart>> @delegate;
        #endregion

        #region Constructor
        /// <summary>
        /// Initialises a new instance of <see cref="PartFactory{T}" />.
        /// </summary>
        /// <param name="delegate">The delegate used to create the part.</param>
        public PartFactory(Func<PartLifetimeContext<TPart>> @delegate)
        {
            if (@delegate == null)
                throw new ArgumentNullException("@deligate");

            this.@delegate = @delegate;
            this.PartType = typeof(TPart);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the <see cref="Type" /> if the part.
        /// </summary>
        public Type PartType { get; private set; }
        #endregion

        #region Methods
        /// <summary>
        /// Creates an instance of the <see cref="PartLifetimeContext{T}" /> used for managing the part.
        /// </summary>
        /// <returns></returns>
        public PartLifetimeContext<TPart> CreatePartLifetimeContext()
        {
            return @delegate();
        }

        /// <summary>
        /// Creates an instance of the part.
        /// </summary>
        /// <returns></returns>
        public TPart CreatePart()
        {
            return CreatePartLifetimeContext().ExportedValue;
        }
        #endregion
    }

    /// <summary>
    /// Provides dynamic creation of parts.
    /// </summary>
    /// <typeparam name="TPart">The type of part to create.</typeparam>
    /// <typeparam name="TMetadata">The type of metadata for the part.</typeparam>
    public class PartFactory<TPart, TMetadata> : PartFactory<TPart>
    {
        #region Constructor
        /// <summary>
        /// Initialises a new instance of <see cref="PartFactory{TPart,TMetadata}" />.
        /// </summary>
        /// <param name="delegate">The delegate used to create the part.</param>
        /// <param name="metadata">The metadata for the part.</param>
        public PartFactory(Func<PartLifetimeContext<TPart>> @delegate, TMetadata metadata)
            : base(@delegate)
        {
            Metadata = metadata;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the metadata for the part.
        /// </summary>
        public TMetadata Metadata { get; private set; }
        #endregion
    }
}
