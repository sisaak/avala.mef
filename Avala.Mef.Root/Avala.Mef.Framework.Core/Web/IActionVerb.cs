﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avala.Mef.Framework.Core.Web
{
    /// <summary>
    /// Defines an action verb.
    /// </summary>
    public interface IActionVerb
    {
        #region Properties
        /// <summary>
        /// Gets the name of the verb.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the action.
        /// </summary>
        string Action { get; }

        /// <summary>
        /// Gets the controller.
        /// </summary>
        string Controller { get; }

        /// <summary>
        /// Gets the area.
        /// </summary>
        string Area { get; }

        /// <summary>
        /// Gets the child actions of the action.
        /// </summary>
        IEnumerable<IActionVerb> Children { get; }
        #endregion

    }
}
