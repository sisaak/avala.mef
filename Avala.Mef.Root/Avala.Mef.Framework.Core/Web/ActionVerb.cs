﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avala.Mef.Framework.Core.Web
{
    public class ActionVerb : IActionVerb
    {
        public string Name
        {
            get;
            set;
        }

        public string Action
        {
            get;
            set;
        }

        public string Controller
        {
            get;
            set;
        }

        public string Area
        {
            get;
            set;
        }

        public IEnumerable<IActionVerb> Children
        {
            get;
            set;
        }
    }
}
