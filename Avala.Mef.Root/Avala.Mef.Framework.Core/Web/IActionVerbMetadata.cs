﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avala.Mef.Framework.Core.Web
{
    /// <summary>
    /// Defines metadata associated with an action verb.
    /// </summary>
    public interface IActionVerbMetadata
    {
        #region Properties
        /// <summary>
        /// Gets the category for the verb.
        /// </summary>
        string Category { get; }

        /// <summary>
        /// Gets the order for the verb.
        /// </summary>
        int Order { get; }
        #endregion
    }
}
