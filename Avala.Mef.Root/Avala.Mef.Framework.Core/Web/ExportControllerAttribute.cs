﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Avala.Mef.Framework.Core.Web
{
    [MetadataAttribute, AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class ExportControllerAttribute : ExportAttribute, IControllerMetadata
    {
        public ExportControllerAttribute(string name, string area = null)
            : base(typeof(IController))
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException("name");

            Name = name;
            Area = area;
        }

        public string Name { get; set; }
        public string Area { get; set; }
    }
}
