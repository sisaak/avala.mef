﻿using System;
using System.Collections.Generic;

namespace Avala.Mef.Framework.Core.Composition
{
    public interface IComposer
    {
        Lazy<T, TMetadata> Resolve<T, TMetadata>();
        Lazy<T, TMetadata> Resolve<T, TMetadata>(string contractName);
        T Resolve<T>();
        T Resolve<T>(string contractName);
        IEnumerable<Lazy<T, TMetadata>> ResolveAll<T, TMetadata>();
        IEnumerable<Lazy<T, TMetadata>> ResolveAll<T, TMetadata>(string contractName);
        IEnumerable<T> ResolveAll<T>();
        IEnumerable<T> ResolveAll<T>(string contractName);
    }
}
