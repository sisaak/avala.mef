﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Avala.Mef.Framework.Core.Composition;
using Microsoft.Practices.Unity;

namespace Avala.Mef.Framework.Core.Injection
{
    public interface IModuleContainer
    {
        void RegisterAllTypes(IUnityContainer container);
    }
}
