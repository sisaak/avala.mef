﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avala.Mef.Services.Core
{
    public interface ILeadService
    {
        string GetLeadName(int id);
    }
}
