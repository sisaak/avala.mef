﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Avala.Mef.Framework.Core.Composition;
using Avala.Mef.Lead.Services;
using Avala.Mef.Services.Core;
using Microsoft.Practices.Unity;

namespace Avala.Mef.Lead.CrossCutting
{
    public class Container
    {
        public void RegisterAllTypes(IUnityContainer container)
        {
            container.RegisterType<ILeadService, LeadService>();
        }
    }
}
