﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Avala.Mef.Framework.Core.Web;

namespace Avala.Mef.Lead.Web.Controllers
{
    [ExportController("LeadReport", "Lead")]
    public class LeadReportController : Controller
    {

        public ActionResult Index()
        {
            ViewBag.Title = "Lead Reports";
            ViewBag.Message = "Lead Reports Index";
            return View();
        }

        public ActionResult BySource()
        {
            ViewBag.Title = "LeadReports";
            ViewBag.Message = "Leads By Source";
            return View();
        }
    }
}
