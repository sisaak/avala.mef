﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Avala.Mef.Framework.Core.Web;
using Avala.Mef.Services.Core;

namespace Avala.Mef.Lead.Web.Controllers
{
    [ExportController("Lead", "Lead")]
    public class LeadController : Controller
    {
        private ILeadService _leadService;

        [ImportingConstructor]
        public LeadController(ILeadService leadService)
        {
            if (leadService == null)
                throw new ArgumentNullException("leadService");
            _leadService = leadService;
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Leads";
            ViewBag.Message = "Lead Index";
            return View();
        }

        public ActionResult Home()
        {
            ViewBag.Title = "Lead";
            ViewBag.Message = _leadService.GetLeadName(0);
            return View();
        }

        public ActionResult Detail(int id)
        {
            ViewBag.Title = "Lead";
            ViewBag.Message = "Lead Detail";
            return View();
        }
    }
}
