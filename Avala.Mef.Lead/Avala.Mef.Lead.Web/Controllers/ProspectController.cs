﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Avala.Mef.Framework.Core.Web;

namespace Avala.Mef.Lead.Web.Controllers
{
    [ExportController("Prospect", "Lead")]
    public class ProspectController : Controller
    {

        public ActionResult Index()
        {
            ViewBag.Title = "Prospects";
            ViewBag.Message = "Prospect Index";
            return View();
        }

        public ActionResult Detail(int id)
        {
            ViewBag.Title = "Prospects";
            ViewBag.Message = "Prospect Detail";
            return View();
        }
    }
}
