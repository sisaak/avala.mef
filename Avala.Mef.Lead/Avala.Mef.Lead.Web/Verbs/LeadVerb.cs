﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Avala.Mef.Framework.Core.Web;

namespace Avala.Mef.Lead.Web.Verbs
{
    [Export(typeof(IActionVerb)), ExportMetadata("Category", "Navigation"), ExportMetadata("Order", 1)]
    public class LeadVerb : ActionVerb
    {
        public LeadVerb()
        {
            Name = "Lead";
            Action = "Home";
            Controller = "Lead";
            Area = "Lead";
            Children = new List<IActionVerb>
            {
                new ActionVerb { Name = "Leads", Action = "Index", Controller = "Lead", Area = "Lead" },
                new ActionVerb { Name = "Prospects", Action = "Index", Controller = "Prospect", Area = "Lead" },
                new ActionVerb { Name = "Reports", Action = "Index", Controller = "LeadReport", Area = "Lead", Children = new List<IActionVerb> {
                    new ActionVerb { Name = "Lead Counts by Source", Action = "BySource", Controller = "LeadReport", Area = "Lead" }
                }}
            };
        }
    }
}
