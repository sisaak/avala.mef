﻿using System.ComponentModel.Composition;
using System.Web.Mvc;

namespace Avala.Mef.Lead.Web
{
    [Export(typeof(AreaRegistration))]
    public class LeadAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Lead";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Lead_default",
                "Lead/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
