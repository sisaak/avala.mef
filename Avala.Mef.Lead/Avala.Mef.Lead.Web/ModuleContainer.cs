﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Avala.Mef.Framework.Core.Composition;
using Avala.Mef.Framework.Core.Injection;
using Avala.Mef.Lead.CrossCutting;
using Microsoft.Practices.Unity;

namespace Avala.Mef.Lead.Web
{
    [Export(typeof(IModuleContainer))]
    public class ModuleContainer : IModuleContainer
    {
        Container _container = new Container();

        public void RegisterAllTypes(IUnityContainer container)
        {
            _container.RegisterAllTypes(container);
        }
    }
}
